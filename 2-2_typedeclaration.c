// header files
#include <stdio.h>

// main method declaration
int main()
{
    // variable declaration 64-bit CPU
    char a;   //(1 bytes) -128 to 127
    short b;  //(2 bytes) -32,768 to 32,767
    int c;    //(4 bytes) -2,147,483,648 to 2,147,483,647
    long d;   //(8 bytes) -9,223,372,036,856,775,808 to 9,223,372,036,856,775,807
    float e;  //(4 bytes) 3.4E +/=38
    double f; //(4 bytes) 1.7E +/=308

    a = 65;
    b = 17;
    c = 17;
    d = 17;
    e = 17.03;
    f = 17.03;

    // body
    printf("Char = %c \n", a);    // print format char
    printf("Short = %d \n", b);   // print format short
    printf("Int = %d \n", c);     // print format int
    printf("Long = %ld \n", d);   // print format long
    printf("Float = %f \n", e);   // print format float
    printf("Double = %lf \n", f); // print format double

    // return statement
    return 0;
}