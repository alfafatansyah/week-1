// header files
#include <stdio.h>
#include <math.h>

// main method declaration
int main()
{
    // variable declaration
    int ad;
    float kot, deta, alpha, beta, gamma;
    ad = 3200;
    kot = 0.0056;
    alpha = 0.0056;
    beta = 0.0056;
    gamma = 0.0056;
    deta = alpha * beta / gamma + 3.2 * 2 / 5;

    int a;
    a = pow(3, 2); // 3 powers 2
    printf("%d \n", a);

    // body
    printf("%f \n", deta);   // default number after coma
    printf("%.2f \n", deta); // 2 number after coma

    // return statement
    return 0;
}