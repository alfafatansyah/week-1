// header files
#include <stdio.h>

// main method declaration
int main()
{
    {
        // variable declaration
        int x = 10, y = 20;
        {
            printf("x = %d, y = %d \n", x, y);
            {
                int y = 40;
                x++;
                y++;
                printf("x = %d, y = %d \n", x, y);
            }
            printf("x = %d, y = %d \n", x, y);
        }
    }

    // return statement
    return 0;
}