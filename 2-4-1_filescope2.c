// header files
#include <stdio.h>
#include "Sub.h"

int num1;

// main method declaration
int main()
{
    printf("%d \n", Sub(10, 5));

    // return statement
    return 0;
}

void func1()
{
    {
        goto label_exec;

        label_exec:;
    }


}
