#include <stdio.h>

int main()
{
    int a = 5, b = 2, c;
    int d = 2;

    c = a + b;
    printf("a+b = %d \n", c);
    c = a - b;
    printf("a-b = %d \n", c);
    c = a * b;
    printf("a*b = %d \n", c);
    c = a / b;
    printf("a/b = %d \n", c);
    d = a % b;
    printf("Remainder when a dividen by b = %d \n", d);

    return 0;
}